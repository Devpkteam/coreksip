USE [master]
GO
/****** Object:  Database [gruposip]    Script Date: 2/5/2020 4:10:09 PM ******/
CREATE DATABASE [gruposip]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'gruposip', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\gruposip.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'gruposip_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\gruposip_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [gruposip] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [gruposip].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [gruposip] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [gruposip] SET ARITHABORT OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [gruposip] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [gruposip] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [gruposip] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [gruposip] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [gruposip] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [gruposip] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [gruposip] SET  DISABLE_BROKER 
GO
ALTER DATABASE [gruposip] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [gruposip] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [gruposip] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [gruposip] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [gruposip] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [gruposip] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [gruposip] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [gruposip] SET RECOVERY FULL 
GO
ALTER DATABASE [gruposip] SET  MULTI_USER 
GO
ALTER DATABASE [gruposip] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [gruposip] SET DB_CHAINING OFF 
GO
ALTER DATABASE [gruposip] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [gruposip] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [gruposip] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [gruposip] SET QUERY_STORE = OFF
GO
USE [gruposip]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [gruposip]
GO
/****** Object:  User [root]    Script Date: 2/5/2020 4:10:10 PM ******/
CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[comentarios]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comentarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[comentario] [varchar](255) NULL,
	[usuario] [varchar](255) NULL,
	[id_data] [int] NULL,
	[fecha_registro] [datetime] NULL,
 CONSTRAINT [PK_comentarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[conexiones]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conexiones](
	[id_autor] [int] NULL,
	[limite_conexion] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cuentas_maestras]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentas_maestras](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cliente] [varchar](255) NOT NULL,
	[tipo_documento] [varchar](255) NULL,
	[documento] [varchar](255) NULL,
	[cuenta] [varchar](255) NULL,
	[especialista] [varchar](255) NULL,
 CONSTRAINT [PK_cuentas_maestras] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gestion_contenidos]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gestion_contenidos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NULL,
	[tipo] [varchar](255) NULL,
	[relacion] [varchar](50) NULL,
 CONSTRAINT [PK_gestion_contenidos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grupos]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grupos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[historial]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[historial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[nombre] [varchar](255) NULL,
	[actividad] [varchar](255) NULL,
	[fecha] [varchar](255) NULL,
	[id_registro] [int] NULL,
	[usuario_afectado] [varchar](255) NULL,
	[sake] [varchar](255) NULL,
 CONSTRAINT [PK_historial] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[meta_usuarios]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[meta_usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[token] [varchar](255) NULL,
	[id_usuario] [int] NULL,
 CONSTRAINT [PK_meta_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movimientos]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movimientos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cliente] [varchar](255) NULL,
	[documento] [varchar](255) NULL,
	[cuenta] [varchar](8000) NULL,
	[especialista] [varchar](255) NULL,
	[relacion] [int] NULL,
	[monto] [money] NULL,
	[monto_retenido] [money] NULL,
	[fecha_registro] [datetime] NOT NULL,
	[sake] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[news]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[news](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_author] [varchar](255) NULL,
	[relacion] [varchar](255) NULL,
	[sake] [varchar](255) NULL,
	[message] [varchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[operaciones_cargadas]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[operaciones_cargadas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_author] [int] NULL,
	[canal] [varchar](255) NULL,
	[tipo_operacion] [varchar](255) NULL,
	[origen] [int] NULL,
	[destino] [int] NULL,
	[fecha_tx] [datetime] NULL,
	[especialista] [varchar](255) NULL,
	[observaciones] [varchar](255) NULL,
	[sake] [varchar](255) NOT NULL,
	[tipo_sake] [varchar](50) NULL,
	[ip] [varchar](255) NULL,
	[ip_no_financiera] [varchar](255) NULL,
	[movimientos] [int] NULL,
	[fecha_registro] [datetime] NULL,
	[cierre] [bit] NULL,
	[estatus] [int] NULL,
	[num_origen] [varchar](255) NULL,
	[num_destino] [varchar](255) NULL,
 CONSTRAINT [PK_operaciones_cargadas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 2/5/2020 4:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NOT NULL,
	[permisos] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 2/5/2020 4:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[clave] [varchar](255) NULL,
	[rol] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
	[correo] [varchar](50) NULL,
	[documento] [int] NULL,
	[estatus] [int] NULL,
	[nombre_equipo] [varchar](255) NULL,
	[nm] [varchar](255) NULL,
	[permisos] [int] NULL,
	[intentos] [int] NULL,
	[rol_permisos] [varchar](500) NULL,
	[grupo] [varchar](50) NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[conexiones] ([id_autor], [limite_conexion]) VALUES (329, 5)
SET IDENTITY_INSERT [dbo].[usuarios] ON 

INSERT [dbo].[usuarios] ([id], [nombre], [apellido], [clave], [rol], [telefono], [correo], [documento], [estatus], [nombre_equipo], [nm], [permisos], [intentos], [rol_permisos], [grupo]) VALUES (329, N'super', N'administrador', N'25d55ad283aa400af464c76d713c07ad', N'sa', N'2', N'sysadmin@gruposip.com', 2, 1, NULL, NULL, 1, 0, N'{"nombre":"sa","CU":true,"EU":true,"BU":true,"ELU":true,"VD":true,"CD":true,"ED":true,"VAD":true,"GC":true,"CC":true,"EC":true,"contador":1}', NULL)
SET IDENTITY_INSERT [dbo].[usuarios] OFF
ALTER TABLE [dbo].[meta_usuarios]  WITH CHECK ADD  CONSTRAINT [FK_meta_usuarios_meta_usuarios] FOREIGN KEY([id])
REFERENCES [dbo].[meta_usuarios] ([id])
GO
ALTER TABLE [dbo].[meta_usuarios] CHECK CONSTRAINT [FK_meta_usuarios_meta_usuarios]
GO
USE [master]
GO
ALTER DATABASE [gruposip] SET  READ_WRITE 
GO
