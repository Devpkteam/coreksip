USE [master]
GO
/****** Object:  Database [gruposip]    Script Date: 2/7/2020 11:17:02 AM ******/
CREATE DATABASE [gruposip]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'gruposip', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\gruposip.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'gruposip_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\gruposip_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [gruposip] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [gruposip].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [gruposip] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [gruposip] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [gruposip] SET ARITHABORT OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [gruposip] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [gruposip] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [gruposip] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [gruposip] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [gruposip] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [gruposip] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [gruposip] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [gruposip] SET  DISABLE_BROKER 
GO
ALTER DATABASE [gruposip] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [gruposip] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [gruposip] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [gruposip] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [gruposip] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [gruposip] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [gruposip] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [gruposip] SET RECOVERY FULL 
GO
ALTER DATABASE [gruposip] SET  MULTI_USER 
GO
ALTER DATABASE [gruposip] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [gruposip] SET DB_CHAINING OFF 
GO
ALTER DATABASE [gruposip] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [gruposip] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [gruposip] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [gruposip] SET QUERY_STORE = OFF
GO
USE [gruposip]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [gruposip]
GO
/****** Object:  User [root]    Script Date: 2/7/2020 11:17:02 AM ******/
CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[comentarios]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comentarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[comentario] [varchar](255) NULL,
	[usuario] [varchar](255) NULL,
	[id_data] [int] NULL,
	[fecha_registro] [datetime] NULL,
 CONSTRAINT [PK_comentarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[conexiones]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conexiones](
	[id_autor] [int] NULL,
	[limite_conexion] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cuentas_maestras]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentas_maestras](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cliente] [varchar](255) NOT NULL,
	[tipo_documento] [varchar](255) NULL,
	[documento] [varchar](255) NULL,
	[cuenta] [varchar](255) NULL,
	[especialista] [varchar](255) NULL,
 CONSTRAINT [PK_cuentas_maestras] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gestion_contenidos]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gestion_contenidos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NULL,
	[tipo] [varchar](255) NULL,
	[relacion] [varchar](50) NULL,
 CONSTRAINT [PK_gestion_contenidos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grupos]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grupos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[historial]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[historial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[nombre] [varchar](255) NULL,
	[actividad] [varchar](255) NULL,
	[fecha] [varchar](255) NULL,
	[id_registro] [int] NULL,
	[usuario_afectado] [varchar](255) NULL,
	[sake] [varchar](255) NULL,
 CONSTRAINT [PK_historial] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[meta_usuarios]    Script Date: 2/7/2020 11:17:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[meta_usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[token] [varchar](255) NULL,
	[id_usuario] [int] NULL,
 CONSTRAINT [PK_meta_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movimientos]    Script Date: 2/7/2020 11:17:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movimientos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cliente] [varchar](255) NULL,
	[documento] [varchar](255) NULL,
	[cuenta] [varchar](8000) NULL,
	[especialista] [varchar](255) NULL,
	[relacion] [int] NULL,
	[monto] [money] NULL,
	[monto_retenido] [money] NULL,
	[fecha_registro] [datetime] NOT NULL,
	[sake] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[news]    Script Date: 2/7/2020 11:17:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[news](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_author] [varchar](255) NULL,
	[relacion] [varchar](255) NULL,
	[sake] [varchar](255) NULL,
	[message] [varchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[operaciones_cargadas]    Script Date: 2/7/2020 11:17:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[operaciones_cargadas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_author] [int] NULL,
	[canal] [varchar](255) NULL,
	[tipo_operacion] [varchar](255) NULL,
	[origen] [int] NULL,
	[destino] [int] NULL,
	[fecha_tx] [datetime] NULL,
	[especialista] [varchar](255) NULL,
	[observaciones] [varchar](255) NULL,
	[sake] [varchar](255) NOT NULL,
	[tipo_sake] [varchar](50) NULL,
	[ip] [varchar](255) NULL,
	[ip_no_financiera] [varchar](255) NULL,
	[movimientos] [int] NULL,
	[fecha_registro] [datetime] NULL,
	[cierre] [bit] NULL,
	[estatus] [int] NULL,
	[num_origen] [varchar](255) NULL,
	[num_destino] [varchar](255) NULL,
 CONSTRAINT [PK_operaciones_cargadas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 2/7/2020 11:17:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NOT NULL,
	[permisos] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 2/7/2020 11:17:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[clave] [varchar](255) NULL,
	[rol] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
	[correo] [varchar](50) NULL,
	[documento] [int] NULL,
	[estatus] [int] NULL,
	[nombre_equipo] [varchar](255) NULL,
	[nm] [varchar](255) NULL,
	[permisos] [int] NULL,
	[intentos] [int] NULL,
	[rol_permisos] [varchar](500) NULL,
	[grupo] [varchar](50) NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[conexiones] ([id_autor], [limite_conexion]) VALUES (329, 5)
SET IDENTITY_INSERT [dbo].[cuentas_maestras] ON 

INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (1, N'VILLEGAS GARCIA SERGIO', N'V', N'18400813', N'01020414370000451264', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (2, N'CONTRALORIA MUNICIPIO XXXXXXXX', N'G', N'00030007070', N'01095454546666466664', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (3, N'MICHELA MATOS JOSE ANTONIO', N'V', N'13333392', N'01020414320000038580', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (4, N'CALDERON OSUNA JEAN GIMI', N'V', N'18888888', N'01020414310100114854', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (5, N'VILLEGAS GARCIA SERGIO', N'V', N'18400813', N'01050134230134225988', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (6, N'VILLEGAS GARCIA SERGIO', N'V', N'18400813', N'01340396163961036846', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (7, N'TERA CAUCHOS C.A', N'J', N'00012345678', N'01095454546636894564', N'Jessica')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (8, N'RUIZ MOROS HENRIETA DEL VALLE', N'V', N'15610610', N'01020414310100095067', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (9, N'ALIMENTOS  CA ', N'J', N'00087654321', N'01095454455664556464', N'Jesus marcano')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (10, N'REA VIZCAINO EDILIA JOSEFA', N'V', N'17000460', N'01020865390000124737', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (11, N'FABRICA YYY  C.A', N'J', N'00001111111', N'01095454546665112264', N'Jesus marcano')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (12, N'GUTIERREZ SAMUEL SANTIAGO', N'V', N'28388400', N'01020635920000134918', N'Arnaldo')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (13, N'MASSAAD ALZAIRI RICARDO ELIAS', N'V', N'13633612', N'01020481010101974052', N'Jessica')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (14, N'TRIAS APONTE ZOREIDA ', N'V', N'22262592', N'01020635940100001958', N'Jesus marcano')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (15, N'FRANCA RUIZ NAILETTE ANGELINA', N'V', N'14444744', N'01020634140100001219', N'Jesus marcano')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (16, N'ESPINO OSTOS ADRIAN JOSE', N'V', N'19300505', N'01020726640000152194', N'Jesus marcano')
INSERT [dbo].[cuentas_maestras] ([id], [cliente], [tipo_documento], [documento], [cuenta], [especialista]) VALUES (17, N'GARCIA  DOMINGUEZ AURELIS DEL VALLE', N'V', N'19790020', N'01020414300000489812', N'Jesus marcano')
SET IDENTITY_INSERT [dbo].[cuentas_maestras] OFF
SET IDENTITY_INSERT [dbo].[gestion_contenidos] ON 

INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (1, N'Home Banking', N'canal', N'')
INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (2, N'Empresarial - Traspaso', N'Tipo operaciones', N'1')
INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (3, N'Alertado', N'sake', N'')
INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (4, N'BDV', N'canal', N'')
INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (5, N'clavenet personal', N'Tipo operaciones', N'4')
INSERT [dbo].[gestion_contenidos] ([id], [nombre], [tipo], [relacion]) VALUES (6, N'Personal - Traspaso', N'Tipo operaciones', N'1')
SET IDENTITY_INSERT [dbo].[gestion_contenidos] OFF
SET IDENTITY_INSERT [dbo].[grupos] ON 

INSERT [dbo].[grupos] ([id], [nombre]) VALUES (1, N'Admin')
SET IDENTITY_INSERT [dbo].[grupos] OFF
SET IDENTITY_INSERT [dbo].[operaciones_cargadas] ON 

INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (1, 329, N'1', N'2', 4, 5, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020414370000451264')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (2, 329, N'4', N'5', 5, 6, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Recibio de la cta fraudadora 01020414370000451264 360,000 bsf', N'30611372', N'3', N'undefined', N'undefined', 5, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01020414370000451264', N'01020414320000038580')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (3, 329, N'4', N'5', 5, 7, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Recibio de la cta fraudadora 01020414370000451264 180,000 bsf', N'30611372', N'3', N'undefined', N'undefined', 5, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01020414370000451264', N'01020414310100114854')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (4, 329, N'4', N'5', 5, 8, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Recibio de la cta fraudadora 01020414370000451264 72,000 bsf', N'30611372', N'3', N'undefined', N'undefined', 5, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01020414370000451264', N'01050134230134225988')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (5, 329, N'4', N'5', 5, 9, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Recibio de la cta fraudadora 01020414370000451264 28,000 bsf', N'30611372', N'3', N'undefined', N'undefined', 5, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01020414370000451264', N'01340396163961036846')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (6, 329, N'1', N'2', 4, 10, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020414320000038580')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (7, 329, N'1', N'2', 4, 11, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020414310100095067')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (8, 329, N'1', N'2', 4, 12, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020865390000124737')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (9, 329, N'1', N'2', 4, 13, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020414370000451264')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (10, 329, N'1', N'2', 4, 14, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Arnaldo', N'Cliente indico haber recibido correo Phishing', N'30611372', N'3', N'186.185.29.106', N'186.185.13.25', 4, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546666466664', N'01020635920000134918')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (11, 329, N'1', N'2', 15, 16, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jessica', N'Cliente indico haber recibido correo Phishing', N'30618183', N'3', N'186.185.13.25', N'186.185.13.25', 15, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546636894564', N'01020481010101974052')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (12, 329, N'1', N'6', 17, 18, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30625876', N'3', N'186.185.12.138', N'186.185.12.138', 17, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454455664556464', N'01020635940100001958')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (13, 329, N'1', N'2', 19, 20, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30622812', N'3', N'200.82.180.88', N'200.82.180.88', 19, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020634140100001219')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (14, 329, N'1', N'2', 19, 21, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30622812', N'3', N'200.82.180.88', N'200.82.180.88', 19, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020726640000152194')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (15, 329, N'1', N'2', 22, 23, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30623052', N'3', N'200.82.180.88', N'200.82.180.88', 22, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020634140100001219')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (16, 329, N'1', N'2', 22, 24, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30623052', N'3', N'200.82.180.88', N'200.82.180.88', 22, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020634140100001219')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (17, 329, N'1', N'2', 22, 25, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30623052', N'3', N'200.82.180.88', N'200.82.180.88', 22, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020726640000152194')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (18, 329, N'1', N'2', 22, 26, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30623052', N'3', N'200.82.180.88', N'200.82.180.88', 22, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020726640000152194')
INSERT [dbo].[operaciones_cargadas] ([id], [id_author], [canal], [tipo_operacion], [origen], [destino], [fecha_tx], [especialista], [observaciones], [sake], [tipo_sake], [ip], [ip_no_financiera], [movimientos], [fecha_registro], [cierre], [estatus], [num_origen], [num_destino]) VALUES (19, 329, N'1', N'2', 22, 27, CAST(N'2016-11-02T00:00:00.000' AS DateTime), N'Jesus marcano', N'Cliente indico haber recibido correo Phishing', N'30623052', N'3', N'200.82.180.88', N'200.82.180.88', 22, CAST(N'2020-02-07T00:00:00.000' AS DateTime), 0, 0, N'01095454546665112264', N'01020414300000489812')
SET IDENTITY_INSERT [dbo].[operaciones_cargadas] OFF
SET IDENTITY_INSERT [dbo].[roles] ON 

INSERT [dbo].[roles] ([id], [nombre], [permisos]) VALUES (1, N'Admin', N'{"nombre":"Admin","CU":true,"EU":true,"BU":true,"ELU":true,"VD":true,"CD":true,"ED":true,"VAD":true,"GC":true,"CC":true,"EC":true,"AS":true,"ANS":true,"contador":1}')
SET IDENTITY_INSERT [dbo].[roles] OFF
SET IDENTITY_INSERT [dbo].[usuarios] ON 

INSERT [dbo].[usuarios] ([id], [nombre], [apellido], [clave], [rol], [telefono], [correo], [documento], [estatus], [nombre_equipo], [nm], [permisos], [intentos], [rol_permisos], [grupo]) VALUES (329, N'super', N'administrador', N'25d55ad283aa400af464c76d713c07ad', N'sa', N'2', N'sysadmin@gruposip.com', 2, 1, NULL, NULL, 1, 0, N'{"nombre":"sa","CU":true,"EU":true,"BU":true,"ELU":true,"VD":true,"CD":true,"ED":true,"VAD":true,"GC":true,"CC":true,"EC":true,"contador":1}', NULL)
SET IDENTITY_INSERT [dbo].[usuarios] OFF
ALTER TABLE [dbo].[meta_usuarios]  WITH CHECK ADD  CONSTRAINT [FK_meta_usuarios_meta_usuarios] FOREIGN KEY([id])
REFERENCES [dbo].[meta_usuarios] ([id])
GO
ALTER TABLE [dbo].[meta_usuarios] CHECK CONSTRAINT [FK_meta_usuarios_meta_usuarios]
GO
USE [master]
GO
ALTER DATABASE [gruposip] SET  READ_WRITE 
GO
