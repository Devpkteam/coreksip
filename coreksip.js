var express = require('express');
var app = express();
var ActiveDirectory = require('activedirectory');
var socketio = require('socket.io');
var crypto = require('crypto');
var sql = require("mssql");
const cors = require("cors");
const bodyParser = require("body-parser");
const multer = require('multer');
const xlsx = require("xlsx");

app.use(cors({ origin: "*" }));

app.use(bodyParser.json());

var countConnect = 0;
var sockets = [];
var config_conex;

var config = {
    user: 'root',
    password: '12345678',
    server: 'localhost',
    database: 'gruposip'
};


var configAD = {
    url: 'ldap://WIN-F8RK1TRBIDQ.osvani.com',
    baseDN: 'cn=Users,dc=osvani,dc=com',
    username: 'administrador@osvani.com',
    password: '12345678Ca'
}
var ad = new ActiveDirectory(configAD);

let usernameAD = 'osvani.colina'
    // Encuentra usuario por nombre de usuario en AD


var db = sql.connect(config, function(err) {
    if (err) console.log(err);
});
app.get('/', function(req, res) {


    // config for your database

    // connect to your database
    // var db = new sql.ConnectionPool(config);

});




if(process.argv.indexOf('--prod') !== -1){
  
    var server = app.listen(5000,'10.30.30.157', () => {
        console.log('Express server puerto 5000, --prod: \x1b[32m%s\x1b[0m', 'online in mode prod');
       
    });
   
}else{


    var server = app.listen(5000, () => {
        console.log('Express server puerto 5000: \x1b[32m%s\x1b[0m', 'online');
       
    });
}

// var server = app.listen(5000, function() {
//     console.log('Server is running..');
// });

function findUserAD(usernameAD, callback) {

    ad.findUser(usernameAD, function(err, user) {
        if (err) {
            callback(null);
        }

        if (!user) callback(null);
        else {
            callback(user)
        }
    });
}

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'uploads')
    },
    filename: (req, file, callBack) => {
        callBack(null, Date.now()+`_${file.originalname}`)
    }
})
  
const upload = multer({ storage: storage })

app.post('/file', upload.single('file'), (req, res, next) => {
    
    const file = req.file;
    
    if (!file) {
        const error = new Error('No File')
        error.httpStatusCode = 400
        return next(error)
    }else{
            console.log(req.body)
            readExcel(file.filename,req.body.author,req.body.today,req.body.nombre).then(()=>{
               
            }).catch((err)=>{
                res.status(200).json({
                    ok: false,
                    err: err,
                    msg: 'error'
                })
            })
    
        
       
    }

})

async function readExcel(excel,author,today,nombre){
    var wb = xlsx.readFile('./uploads/'+excel, {cellDates:true})
    console.log(nombre)
    var ws = wb.Sheets['Transsaciones'];
    
    var excel_datos = xlsx.utils.sheet_to_json(ws)
    verificarCuentaBDDestino(excel_datos,0)
    verificarCuentaBDOrigen(excel_datos,0)
    await guardarMovimientoBD(excel_datos,0,today,author,nombre).then((resp)=>{
       
        if(resp){
            
        }
        
        
    })
    ;

   
}

function verificarCuentaBDDestino(cuentas,i){
    // esta funcion verifica si la cuenta destino existe en la tabla de cuentas_maestras, si no existe se crea
    // FUNCIONA
    
    let sql
    if(i < cuentas.length){
        sql = `SELECT * FROM dbo.cuentas_maestras where cuenta like '${cuentas[i].CUENTA_DESTINO}'`
        query(sql,(msg)=>{
            if(msg.recordset[0]){
                
                verificarCuentaBDDestino(cuentas,i=i+1)
            }else{
                guardarCuentaBDDestino(cuentas,i)
               
            }
        })
    }
}

function guardarCuentaBDDestino(cuentas,i){
    // se guarda la cuenta destino en la base de datos
    let sql = `INSERT INTO dbo.cuentas_maestras (cliente,tipo_documento,documento,cuenta,especialista) VALUES ('${cuentas[i].CLIENTE_DESTINO}','${cuentas[i].TIPO_DOCUMENTO}','${cuentas[i].DOCUMENTO}','${cuentas[i].CUENTA_DESTINO}','${cuentas[i].ESPECIALISTA}')`
    query(sql,(msg)=>{
        verificarCuentaBDDestino(cuentas,i=i+1)
    })
}

function verificarCuentaBDOrigen(cuentas,j){
    // esta funcion verifica si la cuenta origen existe en la tabla de cuentas_maestras, si no existe se crea
    // FUNCIONA
   
    let sql
    if(j < cuentas.length){
        sql = `SELECT * FROM dbo.cuentas_maestras where cuenta like '${cuentas[j].CUENTA_ORIGEN}'`
        query(sql,(msg)=>{
            if(msg.recordset[0]){
                
                verificarCuentaBDOrigen(cuentas,j=j+1)
            }else{
                guardarCuentaBDOrigen(cuentas,j)
                
            }
        })
    }
}

function guardarCuentaBDOrigen(cuentas,j){
    // se guarda la cuenta origen en la base de datos
    let sql = `INSERT INTO dbo.cuentas_maestras (cliente,tipo_documento,documento,cuenta,especialista) VALUES ('${cuentas[j].CLIENTE_ORIGEN}','${cuentas[j].TIPO_DOCUMENTO_ORIGEN}','${cuentas[j].DOCUMENTO_ORIGEN}','${cuentas[j].CUENTA_ORIGEN}','${cuentas[j].ESPECIALISTA}')`
    query(sql,(msg)=>{
        verificarCuentaBDOrigen(cuentas,j=j+1)
    })
}

async function guardarMovimientoBD(cuentas,k,today,author,nombre){
    
    
        if(k < cuentas.length){
           
            let sql;
        if(cuentas[k].BANCO_ORIGEN == 'SI'){
            
            obtenerID(cuentas,k).then((id)=>{
                
                sql = `UPDATE dbo.movimientos SET monto = monto + '${cuentas[k].MONTO_EJECUTADO}' where id = '${id}'`
                query(sql,(msg)=>{
                    sql = `INSERT INTO dbo.movimientos (cliente,documento,cuenta,especialista,relacion,monto,monto_retenido,fecha_registro,sake) VALUES ('${cuentas[k].CLIENTE_DESTINO}','${cuentas[k].TIPO_DOCUMENTO+cuentas[k].DOCUMENTO}','${cuentas[k].CUENTA_DESTINO}','${cuentas[k].ESPECIALISTA}','${id}','${cuentas[k].MONTO_EJECUTADO}','${cuentas[k].RETENIDO_DESTINO}','${today}','${cuentas[k].CASOS_SAKE_ORIGEN}');SELECT SCOPE_IDENTITY() AS id_relacion;`
                    query(sql,(relacion)=>{
                        
                        if(relacion.recordset[0]){guardarOperacionCargada(cuentas,k,id,relacion.recordset[0].id_relacion,author,today,nombre)
                                        }
                    })
                })
            },(err)=>{
                sql = `INSERT INTO dbo.movimientos (cliente,documento,cuenta,especialista,relacion,monto,monto_retenido,fecha_registro,sake) VALUES ('${cuentas[k].CLIENTE_ORIGEN}','${cuentas[k].TIPO_DOCUMENTO_ORIGEN+cuentas[k].DOCUMENTO_ORIGEN}','${cuentas[k].CUENTA_ORIGEN}','${cuentas[k].ESPECIALISTA}',${null},'${cuentas[k].MONTO_EJECUTADO}','0','${today}','${cuentas[k].CASOS_SAKE_ORIGEN}');SELECT SCOPE_IDENTITY() AS id_origen;`
                query(sql,(origen)=>{
                    sql = `INSERT INTO dbo.movimientos (cliente,documento,cuenta,especialista,relacion,monto,monto_retenido,fecha_registro,sake) VALUES ('${cuentas[k].CLIENTE_DESTINO}','${cuentas[k].TIPO_DOCUMENTO+cuentas[k].DOCUMENTO}','${cuentas[k].CUENTA_DESTINO}','${cuentas[k].ESPECIALISTA}','${origen.recordset[0].id_origen}','${cuentas[k].MONTO_EJECUTADO}','${cuentas[k].RETENIDO_DESTINO}','${today}','${cuentas[k].CASOS_SAKE_ORIGEN}');SELECT SCOPE_IDENTITY() AS id_relacion;`
                    query(sql,(relacion)=>{
                        if(origen.recordset[0] && relacion.recordset[0]){
                            guardarOperacionCargada(cuentas,k,origen.recordset[0].id_origen,relacion.recordset[0].id_relacion,author,today,nombre)

                        }
                    })
                })
            })
        }else if(cuentas[k].BANCO_ORIGEN == 'NO'){
            obtenerID(cuentas,k).then((id)=>{
                sql = `INSERT INTO dbo.movimientos (cliente,documento,cuenta,especialista,relacion,monto,monto_retenido,fecha_registro,sake) VALUES ('${cuentas[k].CLIENTE_DESTINO}','${cuentas[k].TIPO_DOCUMENTO+cuentas[k].DOCUMENTO}','${cuentas[k].CUENTA_DESTINO}','${cuentas[k].ESPECIALISTA}','${id}','${cuentas[k].MONTO_EJECUTADO}','${cuentas[k].RETENIDO_DESTINO}','${today}','${cuentas[k].CASOS_SAKE_ORIGEN}');SELECT SCOPE_IDENTITY() AS id_relacion;`
                
                query(sql,(relacion)=>{
                    if(relacion.recordset[0]){guardarOperacionCargada(cuentas,k,id,relacion.recordset[0].id_relacion,author,today,nombre)}
                    
                })
            })
    }
    }else{
        console.log(author,nombre)
        console.log('llegue')
        guardarHistorialCargaMasiva(cuentas,0,author,nombre,today)
        finCargaMasiva()
        
       
        
    
    }
    
    
    
}

async function obtenerID(cuentas,k){
    return await new Promise((resolve,reject)=>{
        let sql = `SELECT id FROM dbo.movimientos WHERE cuenta = '${cuentas[k].CUENTA_ORIGEN}' and sake = '${cuentas[k].CASOS_SAKE_ORIGEN}'`
        query(sql,(id)=>{
            if(id.recordset[0]){
                resolve(id.recordset[0].id)
            }else{
                reject(false)
            }
        })
    })
}



function guardarOperacionCargada(cuentas,k,origen,relacion,author,today,nombre){

    getIdCanal(cuentas[k].CANAL).then((id_canal)=>{
        
        getIdTipoOperacion(cuentas[k].TIPO_DE_OPERACION,id_canal).then((id_operacion)=>{
            getIdTipoSake(cuentas[k].TIPO_SAKE).then((id_tipo_sake)=>{
                let fecha;
                fecha = new Date(cuentas[k].FECHA_TX)
                let fecha_tx;
                fecha_tx = `${fecha.getFullYear()}-${fecha.getMonth()+1}-${fecha.getDate()}`
                let sql = `INSERT INTO dbo.operaciones_cargadas (id_author,canal,tipo_operacion,origen,destino,fecha_tx,especialista,observaciones,sake,tipo_sake,ip,ip_no_financiera,movimientos,fecha_registro,cierre,estatus,num_origen,num_destino) VALUES ('${author}','${id_canal}','${id_operacion}','${origen}',${relacion},'${fecha_tx}','${cuentas[k].ESPECIALISTA}','${cuentas[k].OBSERVACIONES}','${cuentas[k].CASOS_SAKE_ORIGEN}','${id_tipo_sake}','${cuentas[k].IP}','${cuentas[k].IP_NO_FINANCIERA}','${origen}','${today}','${0}','${0}','${cuentas[k].CUENTA_ORIGEN}','${cuentas[k].CUENTA_DESTINO}')`
                query(sql,(msg)=>{
                    
                    guardarMovimientoBD(cuentas,k=k+1,today,author,nombre)
                })
            })

        })
    })
    
}

async function getIdCanal(canal){
    return await new Promise((resolve)=>{
        let sql = `SELECT id FROM dbo.gestion_contenidos where nombre = '${canal}'`
        query(sql,(id)=>{
            if(id.recordset[0]){
                resolve(id.recordset[0].id)
            }else{
                let sql = `INSERT INTO dbo.gestion_contenidos(nombre,tipo,relacion) VALUES ('${canal}','canal','');SELECT SCOPE_IDENTITY() AS id;`;
                query(sql,(id)=>{
                    if(id.recordset[0]){
                        resolve(id.recordset[0].id)
                    }
                })
            }
            
        })
    })
}

async function getIdTipoOperacion(tipo_operacion,id_canal){
    return await new Promise((resolve)=>{
        let sql = `SELECT id FROM dbo.gestion_contenidos where nombre = '${tipo_operacion}' and relacion = '${id_canal}'`
        query(sql,(id)=>{
            if(id.recordset[0]){
                resolve(id.recordset[0].id)
            }else{
                let sql = `INSERT INTO dbo.gestion_contenidos(nombre,tipo,relacion) VALUES ('${tipo_operacion}','Tipo operaciones','${id_canal}');SELECT SCOPE_IDENTITY() AS id;`;
                query(sql,(id)=>{
                    if(id.recordset[0]){
                        resolve(id.recordset[0].id)
                    }
                })
            }
        })
    })
}

async function getIdTipoSake(tipo_sake){
    return await new Promise((resolve)=>{
        let sql = `SELECT id FROM dbo.gestion_contenidos where nombre = '${tipo_sake}'`
        query(sql,(id)=>{
            if(id.recordset[0]){
                resolve(id.recordset[0].id)
            }else{
                let sql = `INSERT INTO dbo.gestion_contenidos(nombre,tipo,relacion) VALUES ('${tipo_sake}','sake','');SELECT SCOPE_IDENTITY() AS id;`;
                query(sql,(id)=>{
                    if(id.recordset[0]){
                        resolve(id.recordset[0].id)
                    }
                })
            }
        })
    })
}

function guardarHistorialCargaMasiva(cuentas,l,author,nombre,today){
    if(l < cuentas.length){
         let sql = `select * from dbo.historial where sake = '${cuentas[l].CASOS_SAKE_ORIGEN}'`
    query(sql,(msg)=>{
        if(!msg.recordset[0]){
            sql = `INSERT INTO dbo.historial (id_usuario,nombre,actividad,fecha,id_registro,usuario_afectado,sake) VALUES ('${author}','${nombre}','carga masiva','${new Date()}','0','${null}','${cuentas[l].CASOS_SAKE_ORIGEN}')`
           console.log(sql)
            query(sql,(msg)=>{
                guardarHistorialCargaMasiva(cuentas,l=l+1,author,nombre,today)
            })
        }else{
            guardarHistorialCargaMasiva(cuentas,l=l+1,author,nombre,today)
        }
    })
    }
   
}

function query(querystring, callback) {
    // create Request object
    var request = new sql.Request();

    // query to the database and get the records
    request.query(querystring, function(err, recordset) {
        if (err) console.log(err)
            // send records as a response
            // //sql.close();
        callback(recordset);
    });

}

var io = socketio.listen(server);

io.on('connection', function(socket) {


    function buildTree(movimientosQuery, operacionesCargadasQuery) {

        /* FORMATO DE BOSQUE
        let TREE_DATA = {
            Groceries: {
                'Almond Meal flour': null,
                'Organic eggs': null,
                'Protein Powder': null,
                Fruits: {
                    Apple: null,
                    Berries: ['Blueberry', 'Raspberry'],
                    Orange: null
                }
            },
            Reminders: [
                'Cook dinner',
                'Read the Material Design spec',
                'Upgrade Application to Angular'
            ]
        };
        */
        treeData = {};
        dataTree = {};
        let movimientosRoot = movimientosQuery.recordset.filter((movimiento) => {
            if (movimiento.relacion) {
                return false;
            } else {
                return true;
            }
        });


        let buildJsonTree = (parentId) => {
            let jsonNode = {};

            let childrensMovimientos = movimientosQuery.recordset.filter((movimiento) => {
                if (parentId == movimiento.relacion) {
                    return true;
                } else {
                    return false;
                }
            });

            childrensMovimientos.forEach((movimiento) => {

                let randomCode = Math.random().toString(36).substring(5);

                //En el caso de que el código aleatorio sea repetido, generamos uno nuevo hasta que sea único.
                while (dataTree.hasOwnProperty(randomCode)) {
                    randomCode = Math.random().toString(36).substring(5);
                }

                let operacionCargada = operacionesCargadasQuery.recordset.filter((operacion) => {
                    if (parentId == operacion.origen && movimiento.id == operacion.destino) {
                        return true;
                    } else {
                        return false;
                    }
                });

               
                dataTree[randomCode] = {
                    name: randomCode,
                    parent: true,
                    root: false,
                    show: true,
                    new: false,
                    edited: false,
                    movimientoId: movimiento.id,
                    operacionCargadaId: operacionCargada[0].id,
                    data: {
                        accountNumber: movimiento.cuenta,
                        name: movimiento.cliente,
                        type_doc: `${movimiento.documento[0]}-`,
                        doc: `${movimiento.documento.slice(1,)}`,
                        amountExecuted: movimiento.monto,
                        amountDetained: movimiento.monto_retenido,
                        commit: '',

                        typeSake: operacionCargada[0].tipo_sake,
                        channel: operacionCargada[0].canal,
                        operation: operacionCargada[0].tipo_operacion,
                        dateTx: operacionCargada[0].fecha_tx,
                        ip: operacionCargada[0].ip,
                        ipNot: operacionCargada[0].ip_no_financiera
                    }
                }

                jsonNode[randomCode] = buildJsonTree(movimiento.id);

            });

            return jsonNode;
        };

        movimientosRoot.forEach((movimientoRoot) => {
            let randomCode = Math.random().toString(36).substring(5);
            let parent;
            //En el caso de que el código aleatorio sea repetido, generamos uno nuevo hasta que sea único.
            while (dataTree.hasOwnProperty(randomCode)) {
                randomCode = Math.random().toString(36).substring(5);
            }
            

            if (movimientoRoot.monto == movimientoRoot.monto_retenido) {
                parent = false;
            } else {
                parent = true;
            }
            
            
            if(operacionesCargadasQuery.recordset[0] !== undefined){
                dataTree[randomCode] = {
                    name: randomCode,
                    parent,
                    root: true,
                    show: true,
                    new: false,
                    edited: false,
                    movimientoId: movimientoRoot.id,
                    data: {
                        accountNumber: movimientoRoot.cuenta,
                        name: movimientoRoot.cliente,
                        type_doc: `${movimientoRoot.documento[0]}-`,
                        typeSake: operacionesCargadasQuery.recordset[0].tipo_sake,
                        doc: `${movimientoRoot.documento.slice(1,)}`,
                        amountExecuted: movimientoRoot.monto,
                        amountDetained: movimientoRoot.monto_retenido,
                        commit: ''
                    }
                }
            }else{
                dataTree[randomCode] = {
                    name: randomCode,
                    parent,
                    root: true,
                    show: true,
                    new: false,
                    edited: false,
                    movimientoId: movimientoRoot.id,
                    data: {
                        accountNumber: movimientoRoot.cuenta,
                        name: movimientoRoot.cliente,
                        type_doc: `${movimientoRoot.documento[0]}-`,
                        doc: `${movimientoRoot.documento.slice(1,)}`,
                        amountExecuted: movimientoRoot.monto,
                        amountDetained: movimientoRoot.monto_retenido,
                        commit: ''
                    }
                }
            }

            
                //Aquí iría el código para rellenar la data de los nodos
            treeData[randomCode] = buildJsonTree(movimientoRoot.id);
            //treeData[randomCode] = {}
        });
        response = {
            treeData,
            dataTree
        }
        return response;
    }


    query("SELECT limite_conexion FROM conexiones", (r) => {
        //sql.close();
        config_conex = r.recordset[0].limite_conexion;
        socket.emit('connection', 'true');
    });
    sockets.push(socket);
    // console.log(sockets);
    function consolelog(dates, linea) {
    }



    socket.on('get_accounts', (data, callback) => {
        var info = 'SELECT * ';

        query(info, (r) => {
            callback(r.recordset);
        });
    });

    socket.on('insert_users', (data, callback) => {
        var pwdHash = crypto.createHash('md5').update(data.dataUser.pwd).digest("hex") + '';
        if (data.dataUser.rol == 'administrador') {
            
            var insert = "INSERT into dbo.usuarios ( nombre, apellido, nm, clave, rol, telefono, correo, documento, estatus, nombre_equipo, permisos, intentos) VALUES ('" + data.dataUser.name + "','" + data.dataUser.lastName + "','" + data.dataUser.nm + "','" + pwdHash + "','" + data.dataUser.rol + "', " + data.dataUser.tlf + " ,'" + data.dataUser.email + "', '" + data.dataUser.doc + "', " + 1 + " , '" + data.dataUser.name_pc + "', 2, 0);SELECT SCOPE_IDENTITY() AS idRecord;"
        } else {
            
            var insert = "INSERT into dbo.usuarios ( nombre, apellido, nm, clave, rol, telefono, correo, documento, estatus, nombre_equipo, permisos, rol_permisos, grupo, intentos) VALUES ('" + data.dataUser.name + "','" + data.dataUser.lastName + "','" + data.dataUser.nm + "','" + pwdHash + "','" + data.dataUser.rol + "', " + data.dataUser.tlf + " ,'" + data.dataUser.email + "', '" + data.dataUser.doc + "', " + 1 + " , '" + data.dataUser.name_pc + "', 3 , '" + data.dataUser.rol_permisos + "', '" + data.dataUser.grupo + "', 0);SELECT SCOPE_IDENTITY() AS idRecord;"
            
        }
        query(insert, (result) => {
            
            var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha,usuario_afectado, sake) VALUES ('" + data.dataUser.id + "', '" + data.dataUser.specialist + "', '" + data.dataUser.activity + "','" + result.recordset[0].idRecord + "' ,'" + data.dataUser.dateToday + "','" + data.dataUser.name + " " + data.dataUser.lastName + "','"+null+"')";
            query(insHist, (r) => {
                callback(result);
            });
        });
    });

    finCargaMasiva = function(){
        
        socket.emit('carga_masiva_ok','ok')
    }

    // socket.on('insert_data', (data, callback) => {
    //     console.log(data.sql)
    //     if (data.sqlMovimientos) {
            
    //         query(data.sqlMovimientos, (resul) => {
    //             callback(resul);
    //         });
    //     } else {
    //         query(data.sql, (resul) => {
    //             var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, sake) VALUES ('" + data.id + "', '" + data.specialist + "', '" + data.activity + "','" + resul.recordset[0].idRecord + "' ,'" + data.dateToday + "', '" + data.sake + "')";
    //             query(insHist, (r) => {
    //                 callback(resul);
    //             });
    //         });
    //     }
    // });

    socket.on('insert_data', (data, callback) => {
        console.log('LA DATA MENOR',data.sql)
        console.log(data.sqlMovimientos)
        if (data.sqlMovimientos) {
            query(data.sqlMovimientos, (resul) => {
                var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, sake) VALUES ('" + data.id + "', '" + data.specialist + "', '" + data.activity + "','" + resul.recordset[0].idRecord + "' ,'" + data.dateToday + "', '" + data.sake + "')";
                console.log(insHist)
                console.log(data)
                query(insHist, (r) => {
                    callback(resul);
                });
            });
        } else {
            query(data.sql, (resul) => {
                console.log('Resul ',resul)
                var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, sake) VALUES ('" + data.id + "', '" + data.specialist + "', '" + data.activity + "','" + resul.recordset[0].idRecord + "' ,'" + data.dateToday + "', '" + data.sake + "')";
                console.log(insHist)
                query(insHist, (r) => {
                    callback(resul);
                });
            });
        }
    });

    socket.on('get_data', (data, callback) => {
        var info = data + '';
        console.log(data)

        query(info, (r) => {
            callback(r.recordset);
        });
    });

    socket.on('insert_roles', (data, callback) => {
        if (data.id) {
            var insRoles = "UPDATE dbo.roles SET nombre = '" + data.data.nombre + "' , permisos = '" + data.data.permisos + "' WHERE id = " + data.id + ";";
            query(insRoles, (r) => {
                callback(r);
            });
        } else {
            var insRoles = "INSERT into dbo.roles (nombre, permisos) VALUES ( '" + data.data.nombre + "', '" + data.data.permisos + "')";
            query(insRoles, (r) => {
                callback(r);
            });
        }
    });

    socket.on('get_roles', (data, callback) => {
        var getRoles;

        getRoles = "SELECT * FROM roles";


        query(getRoles, (r) => {      
            callback(r);
        });
    });

    socket.on('delete_roles', (data, callback) => {
        var deleteRoles;

        deleteRoles = "DELETE FROM roles WHERE id = " + data.data + ";";


        query(deleteRoles, (r) => {        
            callback(r);
        });
    });


    socket.on('insert_grupos', (data, callback) => {
        if (data.id) {
            var insGrupos = "UPDATE dbo.grupos SET nombre = '" + data.data.nombre + "' WHERE id = " + data.id + ";";
            query(insGrupos, (r) => {
                callback(r);
            });
        } else {
            var insGrupos = "INSERT into dbo.grupos (nombre) VALUES ( '" + data.data.nombre + "')";
            query(insGrupos, (r) => {
                callback(r);
            });
        }
    });

    socket.on('get_grupos', (data, callback) => {
        var getGrupos;

        getGrupos = "SELECT * FROM grupos";


        query(getGrupos, (r) => {
            // //sql.close();           
            callback(r);
        });
    });

    socket.on('delete_grupos', (data, callback) => {
        var deleteGrupos;

        deleteGrupos = "DELETE FROM grupos WHERE id = " + data.data + ";";


        query(deleteGrupos, (r) => {
            // //sql.close();           
            callback(r);
        });
    });


    socket.on('get_users', (data, callback) => {
        var getUser;
        if (data == 'empty') {
            getUser = "SELECT * FROM usuarios";
        } else if (data.idUser) {
            getUser = "SELECT * FROM usuarios WHERE id = '" + data.idUser + "'";
        } else if (data.sql) {
            getUser = data.sql + '';
        } else if(data.correoUser){
            getUser = "SELECT * FROM usuarios WHERE correo = '" + data.correoUser + "'";
        }else if(data.nm){
            getUser = "SELECT * FROM usuarios WHERE nm = '" + data.nm + "'";
        }
        query(getUser, (r) => {
            // //sql.close();           
            callback(r.recordset);
        });
    });

    socket.on('update_user', (data, callback) => {
        query(data.sql, (result) => {
            //var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, usuario_afectado,sake) VALUES ('" + data.dataUser.Myid + "', '" + data.dataUser.specialist + "', '" + data.dataUser.activity + "','" + data.dataUser.idRecord + "','" + data.dataUser.dateToday + "','" + data.dataUser.fullName + "','NULL')";
            //query(insHist, (r) => {
                callback(result);
            //});
        });
    });

    socket.on('delete', (data, callback) => {
        query(data.sql, (result) => {
            var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, usuario_afectado) VALUES ('" + data.id + "', '" + data.specialist + "', '" + data.activity + "','" + data.idRecord + "' ,'" + data.dateToday + "','" + data.user + "')";
            query(insHist, (r) => {
                callback(result);
            });
        });
    });


    socket.on('update', (data, callback) => {
        query(data.sql, (result) => {
            if (data.updateCuenta) {
                callback(result);
            } else {
                // var today = new Date();
                // var today = getDate.getFullYear() + "-" + (getDate.getMonth() +1) + "-" + getDate.getDate() ;
                if (data.idRecord) {
                    var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, sake) VALUES ('" + data.id + "', '" + data.name + "', '" + data.activity + "','" + data.idRecord + "' ,'" + data.today + "','" + data.sake + "')";
                } else {
                    var insHist = "INSERT into dbo.historial (id_usuario, nombre, actividad, id_registro,fecha, sake) VALUES ('" + data.id + "', '" + data.name + "', '" + data.activity + "','" + result.recordset[0].idRecord + "' ,'" + data.today + "','" + data.sake + "')";
                }
                query(insHist, (r) => {
                    callback(result);
                });
            }
        });
    });

    socket.on('config', (data, callback) => {
        var sql = "UPDATE conexiones SET limite_conexion = '" + data + "'";
        query(sql, (r) => {
            //sql.close();           
            callback(r);
            config_conex = data;
        });
    });

    socket.on('verify_sake_transactions', (data, callback) => {
        TREE_DATA = {};
        query(`SELECT * FROM movimientos WHERE sake = ${data.sake}`, (movimientosQuery) => {

            if (movimientosQuery.recordset.length === 0) {
                //Retornamos porque significa que este sake no tiene ninguna transacción
                TREE_DATA = {
                    dataTree: {},
                    treeData: {},
                }
                callback(TREE_DATA);
            } else {
                query(`SELECT * FROM operaciones_cargadas WHERE sake = ${data.sake}`, (operacionesCargadasQuery) => {

                    //No verificamos si tiene hijos o no, porque ya teniendo movimientos necesariamente debe tener operaciones_cargadas
                    TREE_DATA = buildTree(movimientosQuery, operacionesCargadasQuery);
                    callback(TREE_DATA)
                });
            }
        })
    });


    socket.on('update_movimiento', (data, callback) => {
        query(`UPDATE dbo.movimientos SET cliente = '${data.cliente}',cuenta = '${data.cuenta}', documento = '${data.documento}', especialista = '${data.especialista}', monto = ${data.monto}, monto_retenido = ${data.monto_retenido} WHERE id = ${data.id}`, (resp) => {
            callback(200);
        });
    });

    socket.on('delete_movimiento', (data, callback) => {
        query(`DELETE FROM dbo.movimientos WHERE id = ${data}`, (resp) => {
            callback(200);
        });

    });

    socket.on('reopen_sake', (data, callback) => {
        query(data.sqlMovimientos, (resp) => {
            callback(200);
        });
    });

    socket.on('update_operacion_cargada', (data, callback) => {
        query(`UPDATE dbo.operaciones_cargadas SET canal = '${data.canal}', tipo_operacion = '${data.tipo_operacion}', fecha_tx = '${data.fecha_tx}', especialista = '${data.especialista}', observaciones = '${data.observaciones}', tipo_sake = '${data.tipo_sake}', ip = '${data.ip}', ip_no_financiera = '${data.ip_no_financiera}', num_origen = '${data.num_origen}', num_destino = '${data.num_destino}' WHERE id= ${data.id}`, (resp) => {
            callback(200);
        })
    });

    socket.on('delete_operacion_cargada', (data, callback) => {
        query(`DELETE FROM dbo.operaciones_cargadas WHERE id = ${data}`, (resp) => {
            callback(200);
        });

    });

    socket.on('get_usuarios_elegibles', (callback) => {
        query(`SELECT id, nombre, apellido, correo FROM dbo.usuarios WHERE LOWER(rol) != 'sa' AND LOWER(rol) != 'administrador' ORDER BY correo ASC`, (resp) => {
            callback(resp.recordset);
        });
    });

    socket.on('update_especialista', (data, callback) => {
        query(`UPDATE dbo.operaciones_cargadas SET especialista = '${data.especialista}', id_author = ${data.idUsuario} WHERE sake = ${data.sake}`, (resp) => {
            query(`UPDATE dbo.movimientos SET especialista = ${data.especialista} WHERE sake = ${data.sake}`, (resp) => {
                callback(200);
            });
        });
    });

    socket.on('loginActiveDirectory', (data, callback) => {
        query(`SELECT * FROM usuarios WHERE nm = '${data.nm}'`, (result) => {
            if (result.recordset.length === 0) {
                response = {
                    status: 404,
                    message: 'El NM no se encuentra registrado en el sistema.'
                }
                callback(response);
            } else if (result.recordset.length === 1) {
                let dataUser = result.recordset[0];
                let usernameAD = dataUser.correo.split('@')[0];
                delete dataUser.clave;
                delete dataUser.documento;
                findUserAD(usernameAD, (responseAD) => {
                    let usuario_bloqueado = false;
                    if (responseAD) {
                        if (responseAD.userAccountControl === '514') {
                            usuario_bloqueado = true;
                        }
                    } else {
                        usuario_bloqueado = true;
                    }

                    if (usuario_bloqueado && dataUser.estatus == 1) {
                        query(`UPDATE usuarios SET estatus = 0 WHERE correo = '${dataUser.correo}'`, (r2) => {
                        });
                    }

                    if (dataUser.intentos >= 3 || dataUser.estatus === 0 || usuario_bloqueado) {
                        response = {
                            status: 403,
                            message: 'El usuario asociado a este NM se encuentra bloqueado, por favor contacte al administrador.'
                        }
                        callback(response);
                    } else {
                        let token = crypto.createHash('md5').update(Math.random() + '').digest("hex") + '';
                        let insertToken = "INSERT into dbo.meta_usuarios ( token, id_usuario ) VALUES ( '" + token + "', " + dataUser.id + ")";
                        query(insertToken, (r2) => {
                            //sql.close();
                            query("UPDATE usuarios SET intentos = 0 WHERE correo = '" + dataUser.correo + "'", (r2) => {
                                socket.token = token;
                                socket.id = dataUser.id;
                                response = {
                                    status: 200,
                                    dataUser
                                }
                                callback(response);
                            });
                        });

                    }

                });
            }


        });
    });

    socket.on('signon', (data, callback) => {

        countConnect++;
        if (countConnect <= config_conex) {
            if (data != undefined) {
                var pwdHash = crypto.createHash('md5').update(data.dataUser.pwd).digest("hex") + '';
                // query("SELECT * FROM usuarios WHERE correo = '" + data.dataUser.email, (result) => {
                query(`SELECT * FROM usuarios WHERE correo = '${data.dataUser.email}'`, (result) => {
                    if (result.recordset.length === 0) {
                        callback("empty");
                    } else {
                        query("SELECT * FROM usuarios WHERE correo = '" + data.dataUser.email + "' AND clave = '" + pwdHash + "'", (r) => {

                            if (r.recordsets[0].length > 0) {
                                var dataUser = {
                                    id: r.recordset[0].id,
                                    status: r.recordset[0].estatus,
                                    permits: r.recordset[0].permisos,
                                    rol: r.recordset[0].rol,
                                    name: r.recordset[0].nombre,
                                    lastName: r.recordset[0].apellido[0],
                                    intentos: r.recordset[0].intentos,
                                    rol_permisos: r.recordset[0].rol_permisos
                                }

                                if (dataUser.intentos >= 3) {
                                    callback("falsed");
                                } else {
                                    var token = crypto.createHash('md5').update(Math.random() + '').digest("hex") + '';
                                    var insertToken = "INSERT into dbo.meta_usuarios ( token, id_usuario ) VALUES ( '" + token + "', " + r.recordset[0].id + ")";
                                    query(insertToken, (r2) => {
                                        //sql.close();
                                        query("UPDATE usuarios SET intentos = 0 WHERE correo = '" + data.dataUser.email + "'", (r2) => {
                                            callback({ dataUser });
                                            socket.token = token;
                                            socket.id = r.recordset[0].id;
                                        });
                                    });
                                }

                            } else {
                                countConnect--;
                                if (data.dataUser.email === 'sysadmin@gruposip.com') {
                                    callback({ mensaje: true });
                                    return;
                                }
                                query("UPDATE usuarios SET intentos = (intentos + 1) WHERE correo = '" + data.dataUser.email + "'", (r) => {
                                    query("SELECT intentos FROM usuarios WHERE correo = '" + data.dataUser.email + "'", (r) => {
                                        if (r.recordset[0].intentos > 2) {
                                            query("UPDATE usuarios SET estatus = 0 WHERE correo = '" + data.dataUser.email + "'", (r) => {
                                                callback('falsed');
                                            });
                                        } else {
                                            callback({ status: false, intentos: r.recordset[0].intentos });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });

            }
        } else {
            countConnect--;
            //sql.close();
            callback("Very users connected");
            socket.disconnect(true);
        }
    });

    socket.on('signon_windows', (data, callback) => {
        countConnect++;
        if (countConnect <= config_conex) {
            query("SELECT * FROM usuarios WHERE nombre_equipo = '" + data + "'", (r) => {
                if (r.recordsets[0].length > 0) {
                    callback({ id: r.recordset[0].id, status: r.recordset[0].estatus });
                } else {
                    callback(false);
                }
            });
        } else {
            countConnect--;
            socket.emit('signon_windows', "Very users connected");
            socket.disconnect(true);
        }
    });

    socket.on('disconnect', function() {
        console.log('disconnect')
        var i = sockets.indexOf(socket);
        sockets.splice(i, 1);
        if (socket.token !== undefined) {
            countConnect--;
        }
    });

    socket.on("error", (error) => {
    });
});